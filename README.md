# MultiCurl Perfomance test

C++ based load test with some validation function.

Features:

- High perfomance (can utilize 10GE and simulate 8000+ watchers)
- Less system requirements (2 core and 4Gb RAM is enough!)
- Can use some sort of scripting 
- And so more (coming soon)


# Some facts:

- Test using libcurl, zlib and boost. Also can be build as static binary (libcurl only one "unstandart" external lib)
- Test use lot of file handler! Check `ulimit -n` before start and set it value to number_of_users + 1%
- Doesn't support Docker

# Usage help

You can just add `-h` key in any place and see all available params. Most basic application: run binary with **filename** as single param. Test will start with defaults and create one watcher thread per one valid line into input file.


| Parametr | Description |
| ------ | ------ |
| -b [ --bench ] | turn off delay between requests |
| -t [ --truncate ] | limit number of workers thread |
| --stop_after_rq |  |
| | |


