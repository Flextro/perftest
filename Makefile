CC=g++
CPPFLAGS=-ggdb2 -Wall -std=gnu++11 -pthread -static-libstdc++ -static-libgcc
LDLIBS=-lcurl -lz /usr/lib/x86_64-linux-gnu/libboost_system.a /usr/lib/x86_64-linux-gnu/libboost_program_options.a /usr/lib/x86_64-linux-gnu/libboost_thread.a /usr/lib/x86_64-linux-gnu/libboost_log.a /usr/lib/x86_64-linux-gnu/libboost_filesystem.a
VPATH=src
SRC_PATH=src

all:	ololo

ololo:  main.o log_func.o curl_func.o vanillaitor.o m3u8_class.o
	$(CC) $(CPPFLAGS) log_func.o curl_func.o vanillaitor.o m3u8_class.o ./src/main.cpp -o imcurl $(LDLIBS)

main: 
	$(CC) $(CPPFLAGS) -c ./src/main.cpp -o main.o $(LDLIBS)

curl:
	$(CC) $(CPPFLAGS) -c curl_func.cpp -o curl_func.o $(LDLIBS)

log:
	$(CC) $(CPPFLAGS) -c log_func.cpp -o log_func.o $(LDLIBS)

xtra:
	$(CC) $(CPPFLAGS) -c vanillaitor.cpp -o vanillaitor.o $(LDLIBS)

m3u8:
	$(CC) $(CPPFLAGS) -c m3u8_class.cpp -o m3u8_class.o $(LDLIBS)

clean:
	rm -rf *.o imcurl

