#ifndef __FUNC__mmmuuuuuuuu__
#define __FUNC__mmmuuuuuuuu__
#include <vector>
#include <string>


class Pltools
{
    public:

         Pltools(){};
         ~Pltools(){};

        char *pl_body;
        struct cont_pl
        {
            std::vector<std::string> chunks;
            std::string last_chunk;
            unsigned int version;
            unsigned int seq_num;
            std::string playlist_url;
            std::string chunk_base_url;
            std::string root_url;
        };

        struct profiles
        {
            unsigned int profile_cnt;
            unsigned int version;
            std::vector<std::string> video;
            std::vector<std::string> audio;
            std::vector<std::string> subs; 
        };
        int get_profiles(char *data, profiles *output, std::string base_url);
        //int chunk_xtract(char *pl_body, cont_pl *parsed_pl);
        int chunk_xtract(char *pl_body, Pltools::cont_pl *parsed_pl);

};

#endif 