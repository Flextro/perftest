#ifndef __FUNC__curl__
#define __FUNC__curl__

#define CURL_BUFFER_SIZE  (16 * 1024) /* 16kB */
#include <curl/curl.h>
#include <zlib.h>
#include <string>
#include <chrono>
#include "m3u8_class.h"


struct thread_structure
{
    CURL *curl;
    std::string url;
    std::string current_pl; //add both video and audio playlist in the future
    long success = 0;
    long fail = 0;
    long thr_dl_speed = 0;
    long total_reqs = 0;
    long dltime_grades[7] = {0, 0, 0, 0, 0, 0, 0};
    double *speed_arr_ptr;
    /* */
    char *curl_cb_data;
    int curl_cb_pos = 0;
    bool curl_cb_finish = false;
    bool is_fresh = false;
    /* */
    unsigned long hash = crc32(0L, Z_NULL, 0); //= crc32(0L, Z_NULL, 0);
    unsigned long hash_old = 1L;
    std::string client;
    Pltools::profiles *master_pl;
    bool sc_mode = false; //script mode
    unsigned int command_position = 0;
    std::vector<std::string> *sc_vec;
    std::chrono::high_resolution_clock::time_point last_change = std::chrono::high_resolution_clock::now();
};

//static size_t curl_write( void *ptr, size_t size, size_t nmemb, void *stream);
int my_trace(CURL *handle, curl_infotype type, char *data, size_t size, void *userp);
void curl_worker(thread_structure *thread_work_struct);
void chunk_to_devnul();

#endif 