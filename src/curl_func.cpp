#include <fstream>
#include <sstream>
#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <thread>
#include <unistd.h>
#include <curl/curl.h>
#include <chrono>
#include <algorithm>
#include <iomanip>
#include <mutex>
#include <deque>
#include <condition_variable>
#include <zlib.h>
#include <iomanip>
#include "curl_func.h"
#include "log_func.h"
#include "m3u8_class.h"
#include <random>


using namespace std;

extern bool load_run;
extern bool verbose;
extern bool bench;
extern bool follow_redir;
extern bool ts_get;
extern long req_period;
extern int curl_req_ret;
extern bool pause_thr;
extern bool bw_test; // make all therad data CB pointed to /dev/null
extern unsigned long chunks_downld_cnt;

extern condition_variable cond_var;
extern mutex m;

extern vector<thread_structure *> all_thr_struct;
extern deque<string> chungs;

static int vario_delay()
{
    try
    {
        random_device rand_dev;
        mt19937 generator(rand_dev());
        uniform_int_distribution<int>  distr(-500, 500);
        return distr(generator) * 100;
    }
    catch (const std::exception&)
    {
        //WARN << "random device is empty";
        return 0;
    }
}

static void dump(const char *text, FILE *stream, unsigned char *ptr, size_t size)
{
    size_t i;
    size_t c;
    unsigned int width=0x10;

    fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n",
    text, (long)size, (long)size);

    for(i=0; i<size; i+= width)
    {
        fprintf(stream, "%4.4lx: ", (long)i);

        /* show hex to the left */
        for(c = 0; c < width; c++)
        {
        if(i+c < size)
            fprintf(stream, "%02x ", ptr[i+c]);
        else
            fputs("   ", stream);
        }

        /* show data on the right */
        for(c = 0; (c < width) && (i+c < size); c++) 
        {
            char x = (ptr[i+c] >= 0x20 && ptr[i+c] < 0x80) ? ptr[i+c] : '.';
            fputc(x, stream);
        }

        fputc('\n', stream); /* newline */
    }
}

int my_trace(CURL *handle, curl_infotype type, char *data, size_t size, void *userp)
{
    const char *text;
    (void)handle; /* prevent compiler warning */
    (void)userp;

    switch (type) 
    {
        case CURLINFO_TEXT:
            fprintf(stderr, "== Info: %s", data);
        default: /* in case a new one is introduced to shock us */
        return 0;

        case CURLINFO_HEADER_OUT:
            text = "=> Send header";
            break;
        case CURLINFO_DATA_OUT:
            text = "=> Send data";
            break;
        case CURLINFO_SSL_DATA_OUT:
            text = "=> Send SSL data";
            break;
        case CURLINFO_HEADER_IN:
            text = "<= Recv header";
            break;
        case CURLINFO_DATA_IN:
            text = "<= Recv data";
            break;
        case CURLINFO_SSL_DATA_IN:
            text = "<= Recv SSL data";
            break;
    }
    dump(text, stderr, (unsigned char *)data, size);
    return 0;
}

/*callback function*/
static size_t curl_write( void *ptr, size_t size, size_t nmemb, void *stream) 
{
    struct thread_structure *result = (struct thread_structure *)stream;
    if (result->curl_cb_finish)
    {
        result->curl_cb_pos = 0;
        result->curl_cb_finish = false;
    }
    /* Will we overflow on this write? */
    if(result->curl_cb_pos + size * nmemb >= CURL_BUFFER_SIZE - 1) {
        ERROR << "curl error: too small buffer";
        return 0;
    }
    /* Copy curl's stream buffer into our own buffer */
    memcpy(result->curl_cb_data + result->curl_cb_pos, ptr, size * nmemb);
    /* Advance the position */
    result->curl_cb_pos += size * nmemb;
    return size * nmemb;
} 

/* - - - - - - - - - - - - - - - - - - */
static void pick_new_url(thread_structure *t_w_s)
{
    if (t_w_s->command_position == t_w_s->sc_vec->size())
        t_w_s->command_position = 0;
    //DEBUG << "command vector size :: " << t_w_s->sc_vec->size();
    std::vector<std::string>::size_type idx = t_w_s->command_position;
    std::string newurl = (*t_w_s->sc_vec)[idx];
    if (newurl.back() == '+')
    {
        newurl.pop_back(); // remote last char ("+")
        newurl += t_w_s->client;
    }
    t_w_s->command_position++;
    curl_easy_setopt(t_w_s->curl, CURLOPT_URL, newurl.c_str());
    t_w_s->url = newurl; //for error handler
    if (verbose)
        DEBUG << t_w_s->client << " picked " << newurl;
}

void curl_worker(thread_structure *t_w_s)
{   
    CURLcode res;
    //curl pre-initializated in main thread, using it
    long http_code;
    double dlsize;
    double dltime;
    double dlspeed;
    double dlspeed_list[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    double worker_avg_speed;
    unsigned int dlspeed_cursor = 0;
    unsigned int dld_time_msec;

    t_w_s->speed_arr_ptr = dlspeed_list;
    t_w_s->success = 0;
    t_w_s->fail = 0;
    t_w_s->total_reqs = 0;

    curl_easy_setopt(t_w_s->curl, CURLOPT_WRITEDATA, t_w_s);  //
    curl_easy_setopt(t_w_s->curl, CURLOPT_SSL_VERIFYPEER, 0L); //skip ssl cert check
    curl_easy_setopt(t_w_s->curl, CURLOPT_NOSIGNAL, 1); //suppress SIGPIPE

    if(t_w_s->sc_mode) //script mode
    {
        FILE *devnull = fopen("/dev/null", "w+");
        curl_easy_setopt(t_w_s->curl, CURLOPT_WRITEDATA, devnull);
    }
    else //...or continuous mode
    {
        curl_easy_setopt(t_w_s->curl, CURLOPT_URL, t_w_s->url.c_str());
        curl_easy_setopt(t_w_s->curl, CURLOPT_WRITEFUNCTION, curl_write); //
    }

    while (load_run)
    {
        if(t_w_s->success == 1 && pause_thr) // pause thread after first success request (login)
        {
            DEBUG << "thread " << t_w_s->url <<" paused!";
            while(pause_thr)
            {
                sleep(1);
            }
            DEBUG << "resuming";
        }
        t_w_s->total_reqs++;
        //req_period += vario_delay();
            //TRACE << "rq period " << req_period; 
        if(not t_w_s->sc_mode)
            usleep(req_period);
        if (t_w_s->sc_mode)
        {
            pick_new_url(t_w_s);
            usleep(req_period + vario_delay());
        }
        res = curl_easy_perform(t_w_s->curl); // all curl magic trigged here
        // get curl_perform result
        curl_easy_getinfo (t_w_s->curl, CURLINFO_RESPONSE_CODE, &http_code);
        curl_easy_getinfo (t_w_s->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &dlsize);
        curl_easy_getinfo (t_w_s->curl, CURLINFO_TOTAL_TIME, &dltime);
        curl_easy_getinfo (t_w_s->curl, CURLINFO_SPEED_DOWNLOAD, &dlspeed);
        t_w_s->thr_dl_speed = dlspeed;

        if (dlsize == 0)
            WARN << t_w_s->url << " body is too small";

        /* null terminate the string */
        t_w_s->curl_cb_data[t_w_s->curl_cb_pos] = '\0';
        t_w_s->curl_cb_finish = true;
        //calc CRC32 for check content change
        t_w_s->hash = crc32(0L, Z_NULL, 0);
        t_w_s->hash = crc32(t_w_s->hash, (const Bytef*)t_w_s->curl_cb_data, t_w_s->curl_cb_pos + 1);
        if(t_w_s->hash != t_w_s->hash_old)
        {   
            ////BOOST_LOG_TRIVIAL(debug) << "Playlist updated. Hash:" << t_w_s->hash << "\t old hash:" << t_w_s->hash_old;
            ////cout << chrono::duration_cast<chrono::milliseconds>(chrono::high_resolution_clock::now() - t_w_s->last_change ).count() << endl;
            t_w_s->last_change = chrono::high_resolution_clock::now(); //renew timer
            t_w_s->is_fresh = true;
            t_w_s->hash_old = t_w_s->hash;
        }

        if (res != CURLE_OK || http_code != 200)
        {
            t_w_s->fail++;
            ERROR << "http_code:" << http_code << "\tcurl_code:" << curl_easy_strerror(res) << "\turl:" << t_w_s->url.c_str();
        }
        else
        {   //if success - calculate speed grade
            t_w_s->success++;
            dld_time_msec = (int)(dltime * 1000);
            if(dld_time_msec > 3000){t_w_s->dltime_grades[6]++;}
            else if (dld_time_msec > 2000){t_w_s->dltime_grades[5]++;}
            else if (dld_time_msec > 1000){t_w_s->dltime_grades[4]++;}
            else if (dld_time_msec > 500){t_w_s->dltime_grades[3]++;}
            else if (dld_time_msec > 100){t_w_s->dltime_grades[2]++;}
            else if (dld_time_msec > 25){t_w_s->dltime_grades[1]++;}
            else {t_w_s->dltime_grades[0]++;}
        }

        //calc avg_speed
        dlspeed_list[dlspeed_cursor++] = dlspeed;
        dlspeed_cursor %= 10; //calculate next position
        worker_avg_speed = 0; //reset before calc
        for (auto tmp_speed : dlspeed_list)
        {
            worker_avg_speed += tmp_speed;
        }
        worker_avg_speed = worker_avg_speed / 10;
    }
}
/* - - - - - - - - - - - - - - - - - - */

void chunk_to_devnul()
{
    CURL *curl_c;
    CURLcode res_c;
    long chunk_code;
    long ok_code;
    string chunk_url;
    curl_c = curl_easy_init();
    unique_lock<mutex> lock(m);
    //check if(curl)... 
    FILE *devnull = fopen("/dev/null", "w+");
    curl_easy_setopt(curl_c, CURLOPT_WRITEDATA, devnull);
    curl_easy_setopt(curl_c, CURLOPT_USERAGENT, "MultiCurl Load Test");
    curl_easy_setopt(curl_c, CURLOPT_TIMEOUT, 15L);
    curl_easy_setopt(curl_c, CURLOPT_CONNECTTIMEOUT, 10L);
    curl_easy_setopt(curl_c, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(curl_c, CURLOPT_NOSIGNAL, 1);
    if (!ts_get)
    {
        curl_easy_setopt(curl_c, CURLOPT_NOBODY, 1L);
    }
    if(follow_redir)
    {
        curl_easy_setopt(curl_c, CURLOPT_FOLLOWLOCATION, 1L);
        ok_code = 302;
    } else
    { ok_code = 200; }
        
    double redir_sz;

    while(load_run)
    {
        if (!chungs.empty())
        {
            chunk_url = chungs.front();
            chungs.pop_front();
            if(verbose)
                INFO << chunk_url;
            curl_easy_setopt(curl_c, CURLOPT_URL, chunk_url.c_str());
            res_c = curl_easy_perform(curl_c);
            curl_easy_getinfo (curl_c, CURLINFO_RESPONSE_CODE, &chunk_code);
            curl_easy_getinfo (curl_c, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &redir_sz);
          
            if (res_c != CURLE_OK || chunk_code != ok_code)
            {
                    ERROR << "not ok condition   http_code:" << chunk_code << "  curl_code:" << curl_easy_strerror(res_c) << "  url:" << chunk_url;    
            }
            else
            {
                chunks_downld_cnt++;
            }
            if(verbose)
                TRACE << chunk_url << "  redir sz is:" << redir_sz;
        }
        else //thread wait unitl job happend
        {
            if(verbose)
                TRACE << "sz " << chungs.size() << "  wait for job";
            cond_var.wait(lock);
        }
        //cout << "prepare for next chunk\n";
    }
        curl_easy_cleanup(curl_c);
}
