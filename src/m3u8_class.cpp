#include "m3u8_class.h"
#include "log_func.h"
#include <iostream>
#include <regex>

using namespace std;

extern bool verbose;

int Pltools::chunk_xtract(char *pl_body, Pltools::cont_pl *parsed_pl)
{
    string pl_body_str = (pl_body);
    const regex base_url("((http|https)://[^/]+)?.+/");   //regex for base url
    smatch match;

    if(regex_search(parsed_pl->playlist_url, match, base_url))
    {
        parsed_pl->chunk_base_url = match[0];
        parsed_pl->root_url = match[1];
    } //get base url
    if (verbose)
    {
    TRACE << "url:\t" << parsed_pl->playlist_url << "\nchunk base:\t"
        << parsed_pl->chunk_base_url << "\nroot url:\t" << parsed_pl->root_url << "\n";
    }
    stringstream ss(pl_body);
    istream_iterator<string> begin(ss);
    istream_iterator<string> end;
    vector<string> pl_strings(begin, end);
    static regex chunk(".+\\.ts.{0,}+"); //regex for chunks names

    for(auto t : pl_strings)
    {
        if (regex_match(t, chunk))
        {
            if (verbose)
                DEBUG << "base:" << parsed_pl->chunk_base_url << "\troot:" << parsed_pl->root_url << "\tt:" << t;
            if(t[0] == '/')
            {
                //chungs.push_back(root_url+t);
                parsed_pl->last_chunk = parsed_pl->root_url+t;
            }else{
                //chungs.push_back(chunk_base_url+t);
                parsed_pl->last_chunk = parsed_pl->chunk_base_url+t;
            }
        }
    }

    return 0;
}

int Pltools::get_profiles(char *data, Pltools::profiles *output, std::string base_url)
{
    string pl = string(data);
   
    if(pl.compare(0,7,"#EXTM3U") != 0)
    {
        cerr << "not a pl!\n";
        return -1;
    }

    stringstream ss(data);
    istream_iterator<string> begin(ss);
    istream_iterator<string> end;
    vector<string> pl_strings(begin, end);
    
    smatch match;
    static regex chunk(".+\\.ts.{0,}+"); //regex for chunks names
    static regex playlist("[a-z0-9_/-]+\\.m3u8");
    static regex tag("^#");
    static regex base_url_rx("((http|https)://[^/]+)?.+/");   //regex for base url in match[0]


    string root_url;
    if(regex_search(base_url, match, base_url_rx))
    {
        root_url = match[0];
    } //get base url

    for(auto t : pl_strings)
    {
        if (regex_search(t, match, chunk))
        {
            TRACE << "playlist contain chunks! it is profile list";
            output->video.push_back(base_url);
            break;
        }
        if (regex_search(t, match, playlist))
        {
            string tmp = match[0];
            if(tmp.compare(0, 4, "http"))
            {
                output->video.push_back(root_url + tmp);
            }
            else
            {
                output->video.push_back(tmp);
            }
            TRACE << "found! " << match[0];
        }
        if (regex_search(t, match, tag)){
            cerr << "tag found: " << t << endl; 
        }
    }
    
    cerr << "pl!\n";

    return 1;
}