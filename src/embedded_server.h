#ifndef __FUNC__esrv__
#define __FUNC__esrv__

#include "../mongoose/mongoose.h"

bool flag_kill = false;

void termination_handler(int)
{
    flag_kill = true;
}

void http_request_handler(struct mg_connection *conn, int ev, void *ev_data);

bool http_srv_init();

#endif 