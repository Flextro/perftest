#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

 
#include <fstream>
#include <iostream>
#include <string>

#include "log_func.h"
#include "vanillaitor.h"

using namespace std;
namespace pt = boost::property_tree;
namespace jsp = pt::json_parser;

void vanillaitor::sig()
{
    INFO << "i wish you were here";
}

void vanillaitor::m3u8_parse()
{
    
}

void vanillaitor::say_code()
{
    const std::string json = R"({"errorCode":0,"result":{"id":"2c6ff1f2-7ef8-454b-a877-d58d38ab2adb","recommendations":[{"channelName":"Первый канал","channelId":138,"startTime":1479981060000,"contentType":"PROGRAM","id":5094436,"name":"3343222442455","srcImgFile":null,"duration":1800,"mediaQuality":"SD"}]}})";

    stringstream stream(json);
    //try here 
    pt::ptree propertyTree;
    pt::read_json(stream, propertyTree);

    BOOST_FOREACH(auto &value, propertyTree)
    {
        INFO << "Prop " << value.first << " : " << value.second.data();
    }

}

/*
void check_json_err(*mem_data)
{
    INFO << "lol";
}
*/