#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <thread>
#include <unistd.h>
#include <iomanip>
#include <exception>
#include <csignal> 
#include <ctime>
#include <mutex>
#include <condition_variable>
#include <zlib.h>
#include <deque>
#include <chrono>
#include <ratio>
#include <regex>
#include <boost/program_options.hpp>
#include <boost/thread.hpp>  
#include "curl_func.h"
#include "log_func.h"
#include "vanillaitor.h"
#include "m3u8_class.h"
#include <sys/resource.h>

//#define mdebug

//#include <boost/program_options/options_description.hpp>
//#include <boost/algorithm/string.hpp>

using namespace std;

volatile bool load_run = true;
bool pause_thr = false;
bool m3u8 = true;
bool ts_get = false;
volatile bool verbose = false;
volatile bool bench = false;
volatile bool follow_redir = false;
bool bw_test = false;
long req_period;
unsigned int stop_after_rq = 0;
unsigned long spawn_nano = 0;
unsigned long chunks_downld_cnt = 0;
vector<boost::thread> workers_thr;
chrono::time_point<std::chrono::high_resolution_clock> start_time = chrono::high_resolution_clock::now();

condition_variable cond_var;
mutex m;

vector<thread_structure *> all_thr_struct;
deque<string> chungs;

namespace po = boost::program_options;

void calc_stat()
{
    long all_success = 0;
    long all_fails = 0;
    long all_speed = 0;
    long all_dlt[7] = {0, 0, 0, 0, 0, 0, 0};
    unsigned int thr_count = 0;
    
    for (auto single_thr_stat : all_thr_struct) // all threads have its own statistic. walk through all of stat and calc summary
    {
        all_success += single_thr_stat->success;
        all_fails += single_thr_stat->fail;
        all_speed += single_thr_stat->thr_dl_speed;
        //calc total success req's
        all_dlt[0] += single_thr_stat->dltime_grades[0];
        all_dlt[1] += single_thr_stat->dltime_grades[1];
        all_dlt[2] += single_thr_stat->dltime_grades[2];
        all_dlt[3] += single_thr_stat->dltime_grades[3];
        all_dlt[4] += single_thr_stat->dltime_grades[4];
        all_dlt[5] += single_thr_stat->dltime_grades[5];
        all_dlt[6] += single_thr_stat->dltime_grades[6];
        thr_count++;
    }

    if ((stop_after_rq > 0) && ((all_success + all_fails) > stop_after_rq)) // move this function to another place... later
    {
        INFO << "total request count reached, stopping test";
        load_run = false;
    }

    auto time_now = chrono::high_resolution_clock::now();
    std::chrono::duration<double, milli>time_to_go = time_now - start_time;
    printf("times (%i thread's run)\n<25:\t%lu\n<100:\t%lu\n<500:\t%lu\n<1s:\t\%lu\n1s+:\t%lu\n2s+:\t%lu\n3s+:\t%lu\n",
        thr_count, all_dlt[0], all_dlt[1], all_dlt[2], all_dlt[3], all_dlt[4], all_dlt[5], all_dlt[6]);
    printf("success/errors : %lu/%lu\n",all_success, all_fails);
    auto rps = all_success / time_to_go.count()*1000 ; 
    cout << "success rate " << rps << " reqs/s"<< endl;
    if(true)
    {
        INFO << chunks_downld_cnt << " chunks dld\n";
    }
    INFO << all_success << "\t/\t" << all_fails << "  success/error's;  avg. speed " << all_speed/(1024) << " kB/s";
    //if(verbose)
        TRACE << "q size " << chungs.size();
     
}

static void periodic_statistic(void)
{
    while (load_run)
    {
        sleep(5);
        calc_stat();
    }
}

//parse input file with simple regex, return all lines contain 'http' 
static vector<string> get_urls(const char* file, unsigned int truncate)
{
    vector<string> lines;
    unsigned int i = 0;
    ifstream infile(file);
    regex re("http.+");

    for( string line; getline( infile, line ); )
    {  
        if (regex_match(line, re)) 
        {
            lines.push_back(line);
            i++;
        }
        if (truncate > 0 && i == truncate)
                break;
    }   
    return lines;
}


static vector<string> get_lines(const char* file)
{
    vector<string> lines;
    ifstream infile(file);
    for( string line; getline( infile, line ); )
    {  
        lines.push_back(line);
    }   
    return lines;
}

void chunk_extractor()
{
    int ret_code;
    Pltools pt;
    struct Pltools::cont_pl parser_stuct;
    //struct thread_structure *union_parser = (struct thread_structure *)union_struct;
    
    while(load_run)
    {
        for(auto t : all_thr_struct)
        {
            if(t->is_fresh)
            {
                t->is_fresh = false;
                if(t->current_pl.length() == 0)
                {
                    TRACE << "current_pl is null for " << t->url;
                    int ret_parser = pt.get_profiles(t->curl_cb_data, t->master_pl, t->url);
                    if(t->master_pl->video.size() > 0)
                    {
                        TRACE << "parser return " << t->master_pl->video.size() << " playlists. here it is:";
                        for (auto u : t->master_pl->video){TRACE << u;}
                        // if not master or not playlist logic goes here
                        t->current_pl = t->master_pl->video.back();
                        curl_easy_setopt(t->curl, CURLOPT_URL, t->current_pl.c_str());
                        TRACE << "new url is " << t->current_pl;
                    }
                }
               
                parser_stuct.playlist_url = t->url;
                ret_code = pt.chunk_xtract(t->curl_cb_data, &parser_stuct);   
            
                if(ret_code == 0)
                {
                    chungs.push_back(parser_stuct.last_chunk);
                }
            }
        }
        if(!chungs.empty())
        {
            cond_var.notify_all();
        }
        usleep(10000);
    }
}

//if ctrl+c... 
void sigintHandler (int signum)
{
    if(load_run)
    {
        load_run = false;
        cout << "\nall threads was stopping now. please wait a few time\n";
        for (boost::thread & t : workers_thr){t.interrupt();}
    } else {cout << "\nthreads stopping in progress, be patient\n";}
}

void sigusrHandler (int signum)
{
    for(auto worker_data_struct : all_thr_struct)
    {
        cout << worker_data_struct->curl_cb_data << endl;
        //parser(worker_data_struct);
    }

}

// static void try_curl_stop()
// {
//     for(auto i : all_thr_struct)
//     {
//         try
//         {
//             curl_easy_setopt(i->curl, CURLOPT_TIMEOUT, 1L);
//             curl_easy_setopt(i->curl, CURLOPT_CONNECTTIMEOUT, 1L);
//         }
//         catch (const std::exception&){}
//     }
// }

int main (int argc, char *argv[])
{
    signal(SIGINT, sigintHandler); // register SIGINT handler for stop test 
    signal(SIGUSR1, sigusrHandler);

    //bool follow_redir;
    string filename;
    bool o_bench;
    bool o_script = false;
    bool o_verbose;
    bool o_follow_redir;
    bool headonly = false;
    bool use_cookies = false;
    //long req_del;
    bool debug_mode = false;
    string script_fn;
    unsigned int truncate = 0;
    bool nochunk = false;
    long curl_timeout;
    std::string curl_ua; 

    po::options_description desc("General options");
    desc.add_options()
        ("help,h", "Show help")
        ("filename", po::value<string>(&filename), "File with test url's")
        ("verbose,v", po::bool_switch(&o_verbose), "More verbose in log and screen")
        ("bench,b", po::bool_switch(&o_bench), "Benchmark mode, no delay")
        ("follow,f", po::bool_switch(&o_follow_redir), "Follow link redirection (302)")
        ("delay,d", po::value(&req_period)->default_value(4000000), "Thread sleep in us")
        ("truncate,t", po::value(&truncate), "Truncate file to n lines" )
        ("stop_after_rq", po::value(&stop_after_rq), "Stop test after n request is done")
        ("usecookies", po::bool_switch(), "Use cookies (for special tests)")
        ("timeout", po::value(&curl_timeout)->default_value(15), "Override curl timeout (in s)")
        ("script", po::value<string>(&script_fn), "Test scripting. Use ./test command_file --script vars_file")
        ("nochunk", po::bool_switch(&nochunk), "Do not request chunks from playlist")
        ("spawn_delay", po::value(&spawn_nano)->default_value(1000), "Delay in uSec before new thread was spawned")
        ("ua", po::value(&curl_ua)->default_value("MultiCurl"), "Override useragent")
        ("headonly", po::bool_switch(&headonly), "Use HEAD method for all requests")
        ("delay_fire", po::bool_switch(&pause_thr), ". . .")
        ("bwtest", po::bool_switch(&bw_test), "do not make recive buffer. all data goes to /dev/null")
        ("tsget", po::bool_switch(&ts_get), "actually GET chunk (HEAD if not selected)")
        ("debug","")
    ;

    po::positional_options_description positionalOptions; 
        positionalOptions.add("filename", 1); 

    po::variables_map vm;
    po::store(po::parse_command_line(argc,argv,desc),vm);
    po::store(po::command_line_parser(argc, argv).options(desc).positional(positionalOptions).run(), vm); 
    po::notify(vm);

    if (vm.count("help")) {
        cout << "MultiCurl Load Test\t\tbuild " << __DATE__ << "  " __TIME__ << endl;
        cout << desc << "\n";
        exit (1);
    }     

    if (vm.count("script"))
        o_script = true;
    if (vm.count("usecookies")){use_cookies = true;}
    if (vm.count("debug")){debug_mode = true;}

    bench = o_bench;
    follow_redir = o_follow_redir;
    verbose = o_verbose;

    cout << "fn is:\t" << filename << endl;
    cout << "t is:\t" << truncate << endl;
    cout << "sar is:\t" << stop_after_rq << endl;

    if(bench) 
        req_period = 10000;

    cout << "req_per=" << req_period << endl;
    vector<string> urls;
    if (o_script)
    {
        urls = get_urls(filename.c_str(), 0); //get urls from list.txt
    }else
    {
        urls = get_urls(filename.c_str(), truncate); //get urls from list.txt
    }

    std::string commandLineStr= "";
    for (int i=1;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));
    INFO << "Test ran with " << commandLineStr << " args";

    TRACE << "file contain " << urls.size() << " urls";

    //check ulimit value
    struct rlimit limit;
    try 
    {
        getrlimit(RLIMIT_NOFILE, &limit);
    }
    catch(const exception& ex){ERROR << "Caught exception: '" << ex.what() << "'";}
    if((limit.rlim_cur + 10) < urls.size())
    {
        long old_limit = limit.rlim_cur;
        limit.rlim_cur = urls.size() + 10;
        WARN << "Current ulimit setting is to low. Try to increase from " << old_limit << " to " << limit.rlim_cur;
        try
        {
            if(setrlimit(RLIMIT_NOFILE, &limit) != 0)
                ERROR << "setrlimit() failed with errno" << errno;
        } 
        catch(const exception& ex) 
        {ERROR << "Caught exception: '" << ex.what() << "'";}
    }

    //start statistic thread first
    boost::thread stat_thr(periodic_statistic);
    stat_thr.detach();

    //spawning workers here
    //vector<boost::thread> workers_thr;
    curl_global_init(CURL_GLOBAL_ALL);
    #ifdef mdebug
        vector<chrono::time_point<std::chrono::high_resolution_clock>> thread_spawn_time;
        thread_spawn_time.push_back(chrono::high_resolution_clock::now());
    #endif


    if(o_script)
    {
        m3u8 = false;
        vector<string> f_patams = get_lines(script_fn.c_str());
        unsigned int spawned = 0;
        for (auto client : f_patams)
        {
            DEBUG << spawned <<" thread spawn for client " << client; 
            thread_structure *thread_strct_ptr = new thread_structure;
            thread_strct_ptr->curl = curl_easy_init();
            if(! thread_strct_ptr->curl)
            {
                ERROR << "Curl init error";
                break;
            }
            curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_COOKIEFILE, "");
            thread_strct_ptr->sc_mode = true;
            thread_strct_ptr->sc_vec = &urls;
            thread_strct_ptr->client = client;

            thread_strct_ptr->curl_cb_data = (char*)malloc(CURL_BUFFER_SIZE);
            if (! thread_strct_ptr->curl_cb_data)
            {
                ERROR << "Error allocating " << CURL_BUFFER_SIZE << " bytes.\n";
                break;
            }


            workers_thr.emplace_back(curl_worker, thread_strct_ptr);
            all_thr_struct.push_back(thread_strct_ptr);
            spawned++;
            if (spawned >= truncate)
                break;
            if (!load_run)
                break;
            if ((spawned % 100) == 0)
                sleep (2);

        }
    }
    else //common spawn algo
    {
        for (auto w_url : urls)
        {
            usleep(1000);
            if (!load_run)
                break;
            thread_structure *thread_strct_ptr = new thread_structure;
            Pltools::profiles *profiles_structure = new Pltools::profiles;
            thread_strct_ptr->master_pl = profiles_structure;
            thread_strct_ptr->curl = curl_easy_init();
            if(! thread_strct_ptr->curl)
            {
                ERROR << "Curl init error for thread with url " << w_url;
                break;
            }
            curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_USERAGENT, curl_ua.c_str());
            curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_TIMEOUT, &curl_timeout);
            curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_CONNECTTIMEOUT, &curl_timeout);
            if (debug_mode)
            {
                curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_DEBUGFUNCTION, my_trace);
                curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_VERBOSE, 1L);
            }
            if(headonly)
            {
                    curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_NOBODY, 1L);
            }
            if (use_cookies)
            {
                curl_easy_setopt(thread_strct_ptr->curl, CURLOPT_COOKIEFILE, "");
            }
            thread_strct_ptr->curl_cb_data = (char*)malloc(CURL_BUFFER_SIZE);
            if (! thread_strct_ptr->curl_cb_data)
            {
                ERROR << "Error allocating " << CURL_BUFFER_SIZE << " bytes.\n";
                break;
            }
            thread_strct_ptr->url = w_url;
            workers_thr.emplace_back(curl_worker, thread_strct_ptr);
            all_thr_struct.push_back(thread_strct_ptr);
            if(spawn_nano > 0 )
            {
                usleep (spawn_nano);
            }
            #ifdef mdebug
                thread_spawn_time.push_back(chrono::high_resolution_clock::now());
            #endif
        }
    } // end of spawner
    #ifdef mdebug
        //calc real spawn time
        auto first_time = thread_spawn_time.front();
        auto prev_tp = first_time;
        for (auto tp : thread_spawn_time)
        {
            cout << chrono::duration_cast<chrono::microseconds>(tp - first_time).count() << "\t" 
                << chrono::duration_cast<chrono::microseconds>(tp - prev_tp).count() << endl;
            prev_tp = tp;
        }
    #endif


    if(verbose)
    {
        for (auto i : all_thr_struct){TRACE << "struct memory begin: " << i << "\tdata memory begin " << &i->curl_cb_data << "\tcurl ptr " << i->curl;}
    }

    if(m3u8 && !nochunk)
    {
        DEBUG << "Chunk requester also started";
        thread validator_thr(chunk_extractor);
        thread chunk_requester(chunk_to_devnul);
        validator_thr.detach();
        chunk_requester.detach();
    }

    if(pause_thr)
    {
        DEBUG << "pause_thr countdown";
        sleep(20);
        pause_thr = false;
        DEBUG << "fired!";
    }

    for (boost::thread & t : workers_thr){t.join();}

    calc_stat(); //calculate final statistic before freeng memory

    try
    {
        for (auto &free_func : all_thr_struct)
        {
            curl_easy_cleanup(free_func->curl);
            free(free_func->curl_cb_data);
        }
        workers_thr.clear();
    }
    catch(const std::exception&){}

    cout << "Bye bye\n";
    return 0;
}