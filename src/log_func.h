#ifndef __FUNC__logger__
#define __FUNC__logger__

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup.hpp>
//#include <boost/log/utility/setup/file.hpp>
//#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sinks/text_multifile_backend.hpp>

#define INFO  BOOST_LOG_SEV(my_logger::get(), boost::log::trivial::info)
#define WARN  BOOST_LOG_SEV(my_logger::get(), boost::log::trivial::warning)
#define ERROR BOOST_LOG_SEV(my_logger::get(), boost::log::trivial::error)
#define STAT  BOOST_LOG_SEV(my_logger::get(), boost::log::trivial::error)
#define TRACE BOOST_LOG_SEV(my_logger::get(), boost::log::trivial::trace)
#define DEBUG BOOST_LOG_SEV(my_logger::get(), boost::log::trivial::debug)

#define SYS_LOGFILE             "DOS.log"

typedef boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level> logger_t;

BOOST_LOG_GLOBAL_LOGGER(my_logger, logger_t)

#endif 